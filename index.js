const express = require("express");
const fs = require("fs");
const exts = require("./allowedExtensions");

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use((req, res, next) => {
  console.log(req.originalUrl, req.method);
  next();
});

app.post("/api/files", async (req, res) => {
  const { filename, content } = req.body;

  if (fs.existsSync("files/" + filename)) {
    return res.status(403).json({ message: "File already exists!" });
  }

  if (!content || !filename) {
    return res.status(400).json({
      message: `Please specify ${
        !content ? "'content'" : "'filename'"
      } parameter`,
    });
  }
  if (exts.includes(filename.split(".")[1])) {
    try {
      await fs.promises.writeFile("files/" + filename, content);
      res.status(200).json({ message: "File created successfully" });
    } catch (error) {
      res.status(500).json({ message: error });
    }
  } else {
    res.status(403).json({ message: "Invalid file extension!" });
  }
});

app.get("/api/files", async (req, res) => {
  try {
    const files = await fs.promises.readdir("files");
    res.status(200).json({ message: "Success", files: files });
  } catch (error) {
    res.status(500).json({ message: error });
  }
});

app.get("/api/files/:filename", async (req, res) => {
  const filename = req.params.filename;
  const filePath = __dirname + "/files/" + filename;
  const files = await fs.promises.readdir("files");

  if (!fs.existsSync("files/" + filename)) {
    return res.status(404).json({ message: `No file with ${filename} found` });
  }

  try {
    fs.readFile(filePath, "utf-8", (error, data) => {
      if (error) throw error;
      res.status(200).json({
        message: "Success",
        filename: filename,
        content: data,
        extension: filename.split(".")[1],
        uploadedDate: fs.statSync(filePath).birthtime,
      });
    });
  } catch (error) {
    res.status(500).json({
      message: "Server Error",
    });
  }
});

app.put("/api/files/modify/:filename", async (req, res) => {
  const { filename: newFileName, content } = req.body;
  const oldFilename = req.params.filename;

  if (!fs.existsSync("files/" + oldFilename)) {
    return res
      .status(404)
      .json({ message: `No file with ${oldFilename} found` });
  }

  if (!content || !newFileName) {
    return res.status(400).json({
      message: `Please specify ${
        !content ? "'content'" : "'filename'"
      } parameter`,
    });
  }
  if (exts.includes(newFileName.split(".")[1])) {
    try {
      const basePath = __dirname + "/files/";
      await fs.promises.writeFile("files/" + oldFilename, content);
      await fs.promises.rename(basePath + oldFilename, basePath + newFileName);
      res.status(200).json({ message: "File modified successfully" });
    } catch (error) {
      res.status(500).json({ message: error });
    }
  } else {
    res.status(403).json({ message: "Invalid file extension!" });
  }
});

app.listen(process.env.PORT || 8080, () => {
  console.log("Server started on 8080");
});
